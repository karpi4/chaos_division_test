﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCubePlane : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        var cube = other.gameObject.GetComponent<Cube>();
        cube?.Destroy();
    }
}
