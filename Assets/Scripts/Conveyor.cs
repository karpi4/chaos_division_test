﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Conveyor : MonoBehaviour
{
    public float Speed;
    public Vector3 Direction;    
    private List<Cube> Cubs = new List<Cube>();

    private void OnCollisionEnter(Collision other)
    {
        var cube = other.gameObject.GetComponent<Cube>();
        if (cube)
        {
            Cubs.Add(cube);
        }
        cube.SetConveyor(this);
    }

    private void OnCollisionExit(Collision other)
    {
        var cube = other.gameObject.GetComponent<Cube>();

        if (cube)
        {
            Cubs.Remove(cube);
        }

        cube.rb.velocity = Speed * Direction * Time.deltaTime;
    }

    void FixedUpdate()
    {
        Cubs.ForEach(cub => cub.Move(Speed, Direction, this));
    }
}
