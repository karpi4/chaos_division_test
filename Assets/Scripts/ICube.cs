﻿using UnityEngine;

public interface ICube
{
   void Move(float speed, Vector3 direction, Conveyor conveyor);
   void Destroy();
}
