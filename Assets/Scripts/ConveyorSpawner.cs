﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class ConveyorSpawner : MonoBehaviour, ISpawner
{
    // Start is called before the first frame update

    public GameObject ConveyorPreview;
    public GameObject ConveyorObject;
    public Camera Camera;

    private bool setConveyorPreview;
    private string PlaneTag = "Plane";
    
    private void Start()
    {
        MenuManager.instance.OnConveyorButtonClick += OnConveyorPreview; 
    }

    private void OnDestroy()
    {
        MenuManager.instance.OnConveyorButtonClick -= OnConveyorPreview; 
    }

    public void OnConveyorPreview()
    {
        setConveyorPreview = !setConveyorPreview;
        ConveyorPreview.SetActive(setConveyorPreview);
    }

    public void Update()
    {
        bool isHitPlane = false;

        if (setConveyorPreview)
        {
            Ray ray = Camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(Camera.transform.position, ray.direction, out var raycastHit, Mathf.Infinity))
            {
                if (raycastHit.collider != null)
                {
                    isHitPlane = true;
                    if (raycastHit.collider.tag.Equals(PlaneTag))
                    {
                        Transform hitTransform = raycastHit.collider.transform;
                        DrowConveyorPreview(hitTransform);
                    }
                }
            }
        }

        if (Input.GetMouseButtonDown(0) && isHitPlane)
        {
            if (ConveyorPreview.activeSelf)
            {
               Spawn();
            }
        }
    }

    public void DrowConveyorPreview(Transform hitTransform)
    {
        Vector3 colliderPosition = hitTransform.position + Vector3.up * 0.5f;
        ConveyorPreview.transform.position = colliderPosition;
    }

    public void Spawn()
    {
        ConveyorPreview.SetActive(false);
        var conveyor = Instantiate(ConveyorObject, transform);
        setConveyorPreview = false;
        conveyor.transform.position = ConveyorPreview.transform.position;
    }

}
