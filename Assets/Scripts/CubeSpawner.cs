﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawner : MonoBehaviour, ISpawner
{
    public GameObject Spawner;

    public List<Transform> spawners = new List<Transform>();
    public GameObject cube;

    private void Start()
    {
        MenuManager.instance.OnCubeSpawnerButtonClick += Spawn; 
    }

    private void OnDestroy()
    {
        MenuManager.instance.OnCubeSpawnerButtonClick -= Spawn; 
    }

    public GameObject CreateSpawner()
    {
        var spawner = Instantiate(Spawner, transform);
        spawners.Add(spawner.transform);
        return spawner;
    }

    public void Spawn()
    {
        foreach (var spawner in spawners)
        {
            var cubeObject = Instantiate(cube, spawner);
        }
    }
}
