﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class PlaneGenerator : MonoBehaviour
{
    public int MapSize = 4;
    public GameObject Plane;
    public CubeSpawner CubeSpawners;
    private int _planeMultiplier = 10;

    void Start()
    {
        Generate();
    }

   public void Generate()
    {
        for (int i = 0; i < MapSize; i++)
        {
            for (int j = 0; j < MapSize; j++)
            {
                var plane = Instantiate(Plane, transform);
                plane.transform.position = new Vector3(j * _planeMultiplier, 0, i * _planeMultiplier);
            }
           var spawner = CubeSpawners.CreateSpawner();
           spawner.transform.position = new Vector3(-_planeMultiplier / 2f, _planeMultiplier / 4f, i * _planeMultiplier);

        }
    }
}
