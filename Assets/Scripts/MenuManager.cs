﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public delegate void ButtonClick(); 
    public event ButtonClick OnConveyorButtonClick;
    public event ButtonClick OnCubeSpawnerButtonClick;
    
    public static MenuManager instance = null; 
    
    void Awake(){

        if (instance == null)
        {
            instance = this;
        }
    }
    
    public void OnConveyor()
    {
        OnConveyorButtonClick?.Invoke();
    }

    public void OnSpawnCube()
    {
        OnCubeSpawnerButtonClick?.Invoke();      
    }

}
