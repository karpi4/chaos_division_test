﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour, ICube
{
    public Rigidbody rb;
    private Conveyor _conveyor;

    public void SetConveyor(Conveyor conveyor)
    {
        _conveyor = conveyor;
    }

    public void Move(float speed, Vector3 direction, Conveyor conveyor)
    {
        //rb.velocity = speed * direction * Time.deltaTime;
        //transform.position = Vector3.MoveTowards(transform.position, direction, speed * Time.deltaTime);
        if (_conveyor == conveyor)
            transform.Translate(direction * speed * Time.deltaTime);
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }


}
